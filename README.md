# zé delivery backend test

O teste foi feito usando a seguinte stack

* java
* maven
* docker
* mongodb
* graphql

## Rodando a aplicação
Para rodar a aplicação, primeiro rode o script `runMongoDBLocal.sh` para subir uma instancia do mongodb e em seguida rode o comando `mvn jetty:run`. A aplicação vai ficar disponível em: `http://localhost:8080`

Existe também o script `buildAndRun.sh` que utiliza o docker-compose para subir toda a aplicação localmente utilizando apenas o docker.

## Deploy
Para realizar o deploy, você pode gerar uma imagem do docker com o seguinte comando `docker build -t zedelivery/backend .` dentro do diretório `ze-delivery-backend`. Dependendo de como a infra estiver configurada, você utilizaria uma ferramenta de deploy como o Jenkins que baixaria a versão mais atual da imagem no registry e realizaria o deploy automaticamente para você.