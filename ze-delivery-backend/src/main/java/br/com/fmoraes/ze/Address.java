package br.com.fmoraes.ze;

import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import java.util.List;

/**
 *
 * @author fmoraes
 */
public class Address {
    private String type;
    private List<Double> coordinates;

    public Address(String type, List<Double> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public Point getPoint() {
        Position position = new Position(coordinates);
        return new Point(position);
    }
}
