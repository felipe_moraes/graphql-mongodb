package br.com.fmoraes.ze;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import java.util.List;

/**
 *
 * @author fmoraes
 */
public class Query implements GraphQLQueryResolver {
    
    private final PdvRepository pdvRepository;

    public Query(PdvRepository pdvRepository) {
        this.pdvRepository = pdvRepository;
    }
    
    public List<Pdv> allPdvs() {
        return this.pdvRepository.getAll();
    }
    
    public Pdv pdv(final String id) {
        return this.pdvRepository.findById(id);
    }
    
    public Pdv findCloser(final Double lat, final Double lng) {
        return this.pdvRepository.findByLatLng(lat, lng);
    }
}
