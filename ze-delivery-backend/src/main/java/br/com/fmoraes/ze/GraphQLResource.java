package br.com.fmoraes.ze;

import com.coxautodev.graphql.tools.SchemaParser;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import graphql.GraphQLError;
import graphql.schema.GraphQLSchema;
import graphql.servlet.DefaultGraphQLErrorHandler;
import graphql.servlet.GraphQLErrorHandler;
import graphql.servlet.SimpleGraphQLServlet;
import java.util.List;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author fmoraes
 */
@WebServlet(urlPatterns = "/graphql")
public class GraphQLResource extends SimpleGraphQLServlet {

    private static final PdvRepository pdvRepository;

    static {
        String mongoURL = System.getenv("MONGO_URL");
        if (mongoURL == null) {
            mongoURL = "localhost";
        }
        MongoDatabase mongo = new MongoClient(mongoURL).getDatabase("zedelivery");
        pdvRepository = new PdvRepository(mongo.getCollection("pdvs"));
    }

    public GraphQLResource() {
        super(buildSchema());
    }

    private static GraphQLSchema buildSchema() {
        return SchemaParser.newParser()
                .file("schema.graphqls")
                .resolvers(new Query(pdvRepository), new Mutation(pdvRepository))
                .build()
                .makeExecutableSchema();
    }

    @Override
    protected GraphQLErrorHandler getGraphQLErrorHandler() {
        return new DefaultGraphQLErrorHandler() {

            @Override
            protected List<GraphQLError> filterGraphQLErrors(List<GraphQLError> errors) {
                return errors;
            }

        };
    }

}
