# Build
mvn clean package && docker build -t br.com.fmoraes/ze-delivery-backend .

# RUN

docker rm -f ze-delivery-backend || true && docker run -d -p 8080:8080 -p 4848:4848 --name ze-delivery-backend br.com.fmoraes/ze-delivery-backend 