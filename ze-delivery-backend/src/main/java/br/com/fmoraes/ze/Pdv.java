package br.com.fmoraes.ze;

/**
 *
 * @author fmoraes
 */
public class Pdv {
    private String id;
    private String tradingName;
    private String ownerName;
    private String document;
    private CoverageArea coverageArea;
    private Address address;

    public Pdv(String tradingName, String ownerName, String document, CoverageArea coverageArea, Address address) {
        this(null, tradingName, ownerName, document, coverageArea, address);
    }

    
    public Pdv(String id, String tradingName, String ownerName, String document, CoverageArea coverageArea, Address address) {
        this.id = id;
        this.tradingName = tradingName;
        this.ownerName = ownerName;
        this.document = document;
        this.coverageArea = coverageArea;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public String getTradingName() {
        return tradingName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getDocument() {
        return document;
    }
    
    public CoverageArea getCoverageArea() {
        return coverageArea;
    }

    public Address getAddress() {
        return address;
    }
}
