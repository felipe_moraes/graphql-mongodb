package br.com.fmoraes.ze;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import static com.mongodb.client.model.Filters.eq;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 *
 * @author fmoraes
 */
public class PdvRepository {

    private final MongoCollection<Document> pdvs;

    public PdvRepository(MongoCollection<Document> pdvs) {
        this.pdvs = pdvs;
        this.pdvs.createIndex(
                Indexes.geo2dsphere("coverageArea"));
        this.pdvs.createIndex(new Document("document", 1), new IndexOptions().unique(true));
    }

    public List<Pdv> getAll() {
        List<Pdv> allPdvs = new ArrayList<>();
        for (Document doc : pdvs.find()) {
            allPdvs.add(pdv(doc));
        }
        return allPdvs;
    }

    public Pdv findById(final String id) {
        Document doc = pdvs.find(eq("_id", new ObjectId(id))).first();
        return pdv(doc);
    }

    public Pdv findByLatLng(final Double lat, final Double lng) {
        Point point = new Point(new Position(Arrays.asList(lat, lng)));
        return pdv(pdvs.find(Filters.near("coverageArea", point, 1000d, 0d)).first());
    }

    void save(Pdv pdv) {
        Document doc = new Document();
        doc.append("tradingName", pdv.getTradingName());
        doc.append("ownerName", pdv.getOwnerName());
        doc.append("document", pdv.getDocument());
        doc.append("coverageArea", coverageAreaToDoc(pdv.getCoverageArea()));
        doc.append("address", addressToDoc(pdv.getAddress()));

        this.pdvs.insertOne(doc);
    }

    private Pdv pdv(Document doc) {
        return new Pdv(
                doc.get("_id").toString(),
                doc.getString("tradingName"),
                doc.getString("ownerName"),
                doc.getString("document"),
                coverageArea(doc.get("coverageArea", Document.class)),
                address(doc.get("address", Document.class)));
    }

    private CoverageArea coverageArea(Document doc) {
        return new CoverageArea(
                doc.getString("type"),
                doc.get("coordinates", List.class));
    }

    private Map<String, Object> coverageAreaToDoc(CoverageArea coverageArea) {
        Map<String, Object> json = new HashMap<>();
        json.put("type", coverageArea.getType());
        json.put("coordinates", coverageArea.getCoordinates());
        return json;
    }

    private Address address(Document doc) {
        return new Address(
                doc.getString("type"),
                doc.get("coordinates", List.class));
    }

    private Map<String, Object> addressToDoc(Address address) {
        Map<String, Object> json = new HashMap<>();
        json.put("type", address.getType());
        json.put("coordinates", address.getCoordinates());
        return json;
    }

    public Pdv findByDocument(String document) {
        Document doc = pdvs.find(eq("document", document)).first();
        return doc == null ? null : pdv(doc);
    }
}
