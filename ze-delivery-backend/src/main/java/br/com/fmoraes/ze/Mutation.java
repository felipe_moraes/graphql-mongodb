package br.com.fmoraes.ze;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import graphql.GraphQLException;
import java.util.List;

/**
 *
 * @author fmoraes
 */
public class Mutation implements GraphQLMutationResolver {
    
    private final PdvRepository pdvRepository;

    public Mutation(PdvRepository pdvRepository) {
        this.pdvRepository = pdvRepository;
    }
    
    public Pdv createPdv(String tradingName, String ownerName, String document, 
            List<List<List<List<Double>>>> coverageArea, List<Double> address) {
        
        Pdv pdv = new Pdv(tradingName, 
                ownerName, document,  new CoverageArea("MultiPolygon", coverageArea),
                new Address("Point", address));
        check(pdv);
        this.pdvRepository.save(pdv);
        return pdv;
    }
    
    private void check(Pdv pdv) throws IllegalArgumentException {
        Pdv existingPdv = pdvRepository.findByDocument(pdv.getDocument());
        if (existingPdv != null) {
            throw new GraphQLException("Pdv with document: " + pdv.getDocument() + " Already exists");
        }
    }
}
