package br.com.fmoraes.ze;

import com.mongodb.client.model.geojson.MultiPolygon;
import com.mongodb.client.model.geojson.PolygonCoordinates;
import com.mongodb.client.model.geojson.Position;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fmoraes
 */
public class CoverageArea {
    private String type;
    private List<List<List<List<Double>>>> coordinates;

    public CoverageArea(String type, List<List<List<List<Double>>>> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public List<List<List<List<Double>>>> getCoordinates() {
        return coordinates;
    }
    
    public MultiPolygon getMultiPolygon() {
        /**
         * [ [ [ -73.958, 40.8003 ], [ -73.9498, 40.7968 ], [ -73.9737, 40.7648 ], [ -73.9814, 40.7681 ], [ -73.958, 40.8003 ] ] ],
            [ [ [ -73.958, 40.8003 ], [ -73.9498, 40.7968 ], [ -73.9737, 40.7648 ], [ -73.958, 40.8003 ] ] ]
         */
        
        List<PolygonCoordinates> polygonCoordinates = new ArrayList<>();
        coordinates.forEach((c) -> {
            c.forEach((intern) -> {
                Position firstPosition = new Position(intern.get(0));
                Position lastPosition = new Position(intern.get(intern.size() - 1));
                List<Position> exterior = Arrays.asList(firstPosition, lastPosition);
                List<Position> holes = new ArrayList<>();
                for (int i = 1; i < intern.size() - 1; i++) {
                    holes.add(new Position(intern.get(i)));
                }
                polygonCoordinates.add(new PolygonCoordinates(exterior, holes));
            });
        });
//        PolygonCoordinates polygonCoordinates = new PolygonCoordinates(exterior, holes);
        return new MultiPolygon(polygonCoordinates);
    }

}
